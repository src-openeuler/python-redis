%global _empty_manifest_terminate_build 0
Name:           python-redis
Version:        5.2.0
Release:        2
Summary:        Python client for Redis key-value store
License:        MIT
URL:            https://github.com/redis/redis-py
Source0:        https://files.pythonhosted.org/packages/source/r/redis/redis-%{version}.tar.gz
Source1:        redis.conf
BuildArch:      noarch
%description
The Python interface to the Redis key-value store.

%package -n python3-redis
Summary:        Python client for Redis key-value store
Provides:       python-redis
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-async-timeout
BuildRequires:  python3-py
BuildRequires:  python3-pytest
BuildRequires:  python3-pytest-asyncio
BuildRequires:  python3-mock
BuildRequires:  redis5
BuildRequires:  python3-numpy
Requires:	python3-async-timeout
Requires:	python3-importlib-metadata
Requires:	python3-typing-extensions
%description -n python3-redis
The Python interface to the Redis key-value store.

%package help
Summary:        Python client for Redis key-value store
Provides:       python3-redis-doc
%description help
The Python interface to the Redis key-value store.

%prep
%autosetup -n redis-%{version}
rm tests/test_commands.py
rm tests/test_bloom.py
rm tests/test_graph.py
rm tests/test_timeseries.py


%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
mkdir -p  %{_builddir}/redis-var/
cp %{SOURCE1} %{_builddir}/redis-var/
sed -i "s#^dir .*#dir %{_builddir}/redis-var#g" %{_builddir}/redis-var/redis.conf
sed -i "s#^logfile .*#logfile %{_builddir}/redis-var/redis.log#g" %{_builddir}/redis-var/redis.conf
redis-server %{_builddir}/redis-var/redis.conf &
CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}" LDFLAGS="${LDFLAGS:-${RPM_LD_FLAGS}}" \
PATH="%{buildroot}%{_bindir}:$PATH" \
PYTHONPATH="${PYTHONPATH:-%{buildroot}%{python3_sitearch}:%{buildroot}%{python3_sitelib}}" \
PYTHONDONTWRITEBYTECODE=1 \
%{?__pytest_addopts:PYTEST_ADDOPTS="${PYTEST_ADDOPTS:-} %{__pytest_addopts}"} \
PYTEST_XDIST_AUTO_NUM_WORKERS=%{_smp_build_ncpus}} \
/usr/bin/pytest -m 'onlynoncluster and not ssl and not redismod' \
--ignore "tests/test_credentials.py" \
--ignore "tests/test_asyncio/test_credentials.py" \
--ignore "tests/test_asyncio/test_cwe_404.py" \
--ignore "tests/test_asyncio/test_cluster.py" \
--ignore "tests/test_ssl.py"
kill %1

%files -n python3-redis -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Mar 13 2025 yaoxin <1024769339@qq.com> - 5.2.0-2
- Switch buildrequire redis to redis5

* Tue Nov 05 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 5.2.0-1
- Upgrade version to 5.2.0
  Extend AggregateRequest with scorer argument
  Pin pytest-profiling version

* Sun Sep 29 2024 yaoxin <yao_xin001@hoperun.com> - 5.1.0-1
- Update to 5.1.0
- Bug Fixes:
  * Handle RESP3 sets as Python lists (#3324)
  * Prevent async ClusterPipeline instances from becoming "false-y" (#3068)
  * Add hostname field to _parse_node_line (#3343)
  * More docs fixes (#3326)
  * Delete the first-defined (and thus "duplicate") Script class (#3333)
  * Catch a known DeprecationWarning when calling .close() (#3335)
  * Add missed redismod at test_commands.py (#3369)
- Breaking Changes:
  * Timeseries insertion filters for close samples (#3228)
  * Enhanced classes string representation (#3001)
  * Partial clean up of Python 3.7 compatibility (#2928)

* Thu Jun 27 2024 wulei <wu_lei@hoperun.com> - 5.0.1-1
- Update package to version 5.0.1

* Thu Jul 13 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 4.6.0-1
- Update package to version 4.6.0

* Thu Apr 6 2023 Dongxing Wang <dxwangk@isoftstone.com> - 4.5.5-1
- upgrade version to 4.5.5

* Mon Aug 09 2021 OpenStack_SIG <openstack@openeuler.org> - 3.5.3-1
- Update version to 3.5.3

* Tue Jan 12 2021 wutao <wutao61@huawei.com> - 3.4.1-1
- upgrade to 3.4.1 adapting to pytest

* Wed Aug 05 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 2.10.6-7
- Remove python2

* Mon Jan 6 2020 qinjian <qinjian18@huawei.com> - 2.10.6-6
- Package init

